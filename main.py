import json
from pprint import pprint
from termcolor import colored
from statistics import mean
from statistics import median
import math

with open('people.json', 'r') as p:
    people = json.loads(p.read())

print(colored("""
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   
""", 'yellow'))
print(colored('Modele des données :', 'yellow'))
pprint(people[0])

# debut de l'exo
print(colored(''.join(['_' for _ in range(80)]), 'green', 'on_green'))

print(colored("Nombre d'hommes : ", 'yellow'))
# pour chaque personne du tableau, si son genre == 'Male' je le met dans le tableau hommes
hommes = [p for p in people if p['gender'] == 'Male']
# len() revoie la taille (nombre d'élément) d'un tableau
pprint(len(hommes))

################################################################################

# je peux aussi l'écrire avec une boucle classique
hommes2 = []                        # un tableau vide
for person in people:               # pour chaque persone du tableau
    if person["gender"] == "Male":  # si c'est un homme (2-266-02250-4)
        hommes2.append(person)      # je l'ajoute au tableau
print(len(hommes2))

################################################################################

# dans la même idée, plutot que de mettre tous les hommes dans un tableau
# puis afficher la longueur du tableau, je peux juste les compter dans une variable
nb_hommes = 0                       # je commence à 0
for person in people:               # pour chaque persone du tableau
    if person["gender"] == "Male":  # si c'est un homme
        nb_hommes = nb_hommes + 1   # j'ajoute 1 à mon compteur
print(nb_hommes)

################################################################################

print(colored("Nombre de femmes : ", 'yellow'))
# je peux compter les femmes ou calculer : nombre d'élement dans people - nombre d'homme
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

nm_femmes= 0
for person in people:
    if person ["gender"] == "Female":
        nm_femmes= nm_femmes+1
        print (nm_femmes)

################################################################################

print(colored("Nombre de personnes qui cherchent un homme :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

nmpersonnes_hommes= 0
for person in people:
    if person["looking_for"] == "M":
        nmpersonnes_hommes=nmpersonnes_hommes+1
        print(nmpersonnes_hommes)
################################################################################

print(colored("Nombre de personnes qui cherchent une femme :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

nmpersonne_femmes= 0
for person in people:
    if person["looking_for"] =="F":
        nmpersonne_femmes=nmpersonne_femmes+1
        print(nmpersonne_femmes)

################################################################################

print(colored("Nombre de personnes qui gagnent plus de 2000$ :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))
nbpersonnes_argent= -197
for person in people:
    if person ["income"]>="$2000.00":
        nbpersonnes_argent= nbpersonnes_argent+1

print (nbpersonnes_argent)


################################################################################

print(colored("Nombre de personnes qui aiment les Drama :", 'yellow'))

nbpersonnes_dramas= 0
for person in people:
    if "Drama" in person["pref_movie"]:
        nbpersonnes_dramas= nbpersonnes_dramas+1

print (nbpersonnes_dramas)

# là il va falloir regarder si le chaine de charactères "Drama" se trouve dans "pref_movie"
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Nombre de femmes qui aiment la science-fiction :", 'yellow'))
# si j'ai déjà un tableau avec toutes les femmes, je peux chercher directement dedans ;)
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

nbpersonnes_sciencefiction= 0
for person in people:
    if person["gender"] == "Female" and "Sci-Fi" in person["pref_movie"]:
        nbpersonnes_sciencefiction= nbpersonnes_sciencefiction+1
    
   


    print(nbpersonnes_sciencefiction)


################################################################################

print(colored('LEVEL 2' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))

################################################################################

print(colored("Nombre de personnes qui aiment les documentaires et gagnent plus de 1482$", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))


nbdocumentaires__1482= 0
for person in people:
    if "Documentary" in person["pref_movie"] and float(person['income'][1:])> 1482:

        nbdocumentaires__1482= nbdocumentaires__1482+1




print(nbdocumentaires__1482)

################################################################################

print(colored("Liste des noms, prénoms, id et revenus des personnes qui gagnent plus de 4000$", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

nb__4000= 0
for person in people:
    if  "last_name" in person and ["first_name"] and float(person['income'][1:])> 4000:
        print (person)
        nb__4000= nb__4000+1




print (nb__4000)


################################################################################

print(colored("Homme le plus riche (nom et id) :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

income_max=0
personne_plusriche={}
for person in hommes:
       if income_max< float(person['income'][1:]):
                income_max= float(person['income'][1:])
                personne_plusriche= person




print(personne_plusriche['id'],personne_plusriche['last_name'])
                
                        
   
    

################################################################################

print(colored("Salaire moyen :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))



total=0
for person in people:
    total = total + float(person["income"][1:])
m=total / len(people)



print(m)






################################################################################

print(colored("Salaire médian :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

list_income=[]
for person in people:
    list_income.append(float(person['income'][1:]))




print(median(list_income))


################################################################################

print(colored("Nombre de personnes qui habitent dans l'hémisphère nord :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

people_in_north = []

for person in people:
    if person["latitude"] > 0:
        people_in_north.append(person)

print(len(people_in_north))


################################################################################

print(colored("Salaire moyen des personnes qui habitent dans l'hémisphère sud :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

peoplein_south=[]
list_median=[]

for person in people:
    if person["latitude"] <0:
        peoplein_south.append(person) 
        list_median.append(float(person['income'][1:]))
print(list_median)
print(mean(list_median))


################################################################################

print(colored('LEVEL 3' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))

################################################################################

print(colored("Personne qui habite le plus près de Bérénice Cawt (nom et id) :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

print(colored("Personne qui habite le plus près de Bérénice Cawt (nom et id) :", 'yellow'))

def distance(p1, p2):
    B = p1["latitude"] - p2["latitude"]
    C = p1["longitude"] - p2["longitude"]
    return math.sqrt(B**2 + C**2)

def recup_plus_proche(prenom, nom):
    ref = {}
    for person in people:
        if person['first_name'] == prenom and person["last_name"] == nom:
            ref = person
            
    distance_min = 0
    le_plus_proche = {}
    for person in people:
        if person != ref:
            dis = distance(ref, person)
            if distance_min == 0:
                distance_min = dis
            elif dis < distance_min:
                distance_min = dis
                le_plus_proche = person
    return le_plus_proche
le_plus_proche_de_berenice = recup_plus_proche("Bérénice", "Cawt")
print(le_plus_proche_de_berenice['id'], le_plus_proche_de_berenice['last_name'])

################################################################################

print(colored("Personne qui habite le plus près de Ruì Brach (nom et id) :", 'yellow'))

le_plus_proche_de_Rui = recup_plus_proche("Ruì", "Brach")
print(le_plus_proche_de_Rui["id"], le_plus_proche_de_Rui['last_name'])
        



################################################################################

print(colored("Personne qui habite le plus près de Ruì Brach (nom et id) :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

def distance(p1, p2):
    B = p1["latitude"] - p2["latitude"]
    C = p1["longitude"] - p2["longitude"]
    return math.sqrt(B + C)

Ruì = {}
for person in people:
    if person['first_name'] == "Ruì" and person["last_name"] == "Brach":
        Ruì= person


distances = []
distance_min = 1000
le_plus_proche = {}
for person in people:
    if person != Ruì :
        dis= distances(Ruì,person)
        distances.append(dis)
        if dis < distance_min:
            distance_min = dis
            le_plus_proche = person
print(distances)
print(le_plus_proche["id"], le_plus_proche["last_name"])

################################################################################

print(colored("les 10 personnes qui habitent les plus près de Josée Boshard (nom et id) :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

def distance(p1, p2):
    B = p1["latitude"] - p2["latitude"]
    C = p1["longitude"] - p2["longitude"]
    return math.sqrt(B + C)

Josée = {}
for person in people:
    if person['first_name'] == "Josée" and person["last_name"] == "Boshard":
        Josée= person


distances = []
distance_min = 1000
les_plus_proches = {10}
for person in people:
    if person != Josée:
        dis = distances(Josée,person)
        distances.append(dis)
        if dis < distance_min:
            distance_min = dis
            les_plus_proches = person
print(distances)
print(les_plus_proches["id"], les_plus_proches["last_name"])


################################################################################

print(colored("Les noms et ids des 23 personnes qui travaillent chez google :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Personne la plus agée :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Personne la plus jeune :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))
print(colored("Moyenne des différences d'age :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored('LEVEL 4' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))
print(colored("Genre de film le plus populaire :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Genres de film par ordre de popularité :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Liste des genres de film et nombre de personnes qui les préfèrent :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Age moyen des hommes qui aiment les films noirs :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Age moyen des femmes qui aiment les drames et habitent sur le fuseau horaire, de Paris : ", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("""Homme qui cherche un homme et habite le plus proche d'un homme qui a au moins une
préférence de film en commun (afficher les deux et la distance entre les deux):""", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored("Liste des couples femmes / hommes qui ont les même préférences de films :", 'yellow'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))

################################################################################

print(colored('MATCH' + ''.join(['_' for _ in range(73)]), 'green', 'on_green'))
"""
    On match les gens avec ce qu'ils cherchent (homme ou femme).
    On prend en priorité ceux qui ont le plus de gouts en commun.
    Puis ceux qui sont les plus proches.
    Les gens qui travaillent chez google ne peuvent qu'être en couple entre eux.
    Quelqu'un qui n'aime pas les Drama ne peux pas être en couple avec quelqu'un qui les aime.
    Quelqu'un qui aime les films d'aventure doit forcement être en couple avec quelqu'un qui aime aussi 
    les films d'aventure.
    La différences d'age dans un couple doit être inférieure à 25% (de l'age du plus agé des deux)
    ߷    ߷    ߷    Créer le plus de couples possibles.                  ߷    ߷    ߷    
    ߷    ߷    ߷    Mesurez le temps de calcul de votre fonction         ߷    ߷    ߷    
    ߷    ߷    ߷    Essayez de réduire le temps de calcul au maximum     ߷    ߷    ߷    

"""
print(colored("liste de couples à matcher (nom et id pour chaque membre du couple) :", 'yellow'))
print(colored('Exemple :', 'green'))
print(colored('1 Alice A.\t2 Bob B.'))
print(colored('A IMPLEMENTER', 'red', 'on_yellow'))
